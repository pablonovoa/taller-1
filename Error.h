#ifndef ERROR_H_INCLUDED
#define ERROR_H_INCLUDED

#include <stdio.h>

typedef enum {
    NO_INPUT,
    WRONG_COMMAND,
    WRONG_PARAMS_COUNT,
    EXPRESION_INVALIDA,
    LISTA_VACIA,
    EXPRESION_NO_ENCONTRADA,
    NO_ES_NUM,
    ERROR_GENERICO
} Error;

void tirarError(int e);


#endif // ERROR_H_INCLUDED
