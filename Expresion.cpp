#include "Expresion.h"

void Crear(Expresion &e){ e = NULL; }
bool EsVacio (Expresion e){ return e==NULL; }
Expresion HijoIzq (Expresion e){ return e->hizq; }
Expresion HijoDer (Expresion e){ return e->hder; }

Expresion crearExpAtomica(string s){
    bool isNum = strIsNum(s);
    char op = s[0];

    Expresion e;
    Crear(e);

    if(!isNum && (op != 'x' || strlar(s) != 1) ){
        tirarError(EXPRESION_INVALIDA);
    }else{
        e = new NodoExp;
        e->esNum = isNum;
        e->hizq = NULL;
        e->hder = NULL;
        if(e->esNum){
                e->valor.num = strAnum(s);
        }else{
            e->valor.operador = op;
        }
    }

   return e;
}

Expresion crearExpOperador(char op, Expresion expIzq, Expresion expDer){

    Expresion e = new NodoExp;
    e->valor.operador = op;
    e->esNum = false;
    e->hizq = expIzq;
    e->hder = expDer;

    return e;
}

void mostrarExpresion(Expresion e){
    if(e!=NULL){
        if(e->esNum)
            if(e->valor.num < 0)
                printf("(%i)", e->valor.num);
            else
                printf("%i", e->valor.num);
        else if(e->valor.operador == 'x')
            printf("x");
        else{
            printf("(");
            mostrarExpresion(e->hizq);
            printf("%c", e->valor.operador);
            mostrarExpresion(e->hder);
            printf(")");
        }
    }

}

int evalExp(Expresion e, int x){
    if(e->esNum)
        return e->valor.num;
    else if(e->valor.operador == 'x')
        return x;
    else if(e->valor.operador == '+')
        return evalExp(e->hizq, x) + evalExp(e->hder, x);
    else if(e->valor.operador == '*')
        return evalExp(e->hizq, x) * evalExp(e->hder, x);
    else{
        tirarError(ERROR_GENERICO);
        return 0;
    }
}

bool equalsExp(Expresion e1, Expresion e2){
    if(e1 == NULL && e2 == NULL)
        return true;

    if(e1 != NULL && e2 != NULL){
        if(equalsNodo(e1, e2) && equalsExp(e1->hizq, e2->hizq) && equalsExp(e1->hder, e2->hder)){
            return true;
        }
    }
    return false;
}

bool equalsNodo(Expresion e1, Expresion e2){
    bool sonIguales = false;
    if(
        (e1->esNum && e2->esNum && e1->valor.num == e2->valor.num)
        ||
        (!e1->esNum && !e2->esNum && e1->valor.operador == e2->valor.operador)
     ){ sonIguales = true; }
    return sonIguales;
}

void borrarExp(Expresion &e){
    if(e!=NULL)
        delete e;
}


void Bajar_Exp_Aux(Expresion e, FILE * f){
    if(e != NULL){
        fwrite(&(e->esNum), sizeof(bool), 1, f);
        if(e->esNum)
            fwrite(&(e->valor.num), sizeof(int), 1, f);
        else
            fwrite(&(e->valor.operador), sizeof(char), 1, f);

        Bajar_Exp_Aux(e->hizq, f);
        Bajar_Exp_Aux(e->hder, f);
    }
    /*

    */
    bool b=true;
    int i=e->valor.num;
    fwrite(&(e->esNum), sizeof(bool), 1, f);
    fwrite(&(e->valor.num), sizeof(int), 1, f);

}

void Bajar_Exp(string nomArch, Expresion e){
    FILE * f = fopen("qwe.data", "wb");
    Bajar_Exp_Aux(e, f);
    fclose(f);

    /*
    FILE * f2 = fopen("qwe.data", "rb");
    bool b2;
    int i2;
    char c2;
    fread(&b2, sizeof(bool), 1, f2);
    if(b2)
        fread(&i2, sizeof(int), 1, f2);
    else
        fread(&c2, sizeof(char), 1, f2);

    fclose(f2);
    if(b2)
        printf("\n%i",i2);
    else
        printf("\n%c",c2);

    FILE * f2 = fopen("qwe.data", "rb");
    Expresion eLoad;
    Levantar_Exp_Aux(eLoad, f);
    fclose(f2);
    */





}
void Levantar_Exp(){

    printf("22222");
    FILE * f = fopen("qwe.data", "rb");
    bool b2;
    int i2;
    fread(&b2, sizeof(bool), 1, f);
    fread (&i2, sizeof(int), 1, f);
    fclose (f);
    /*


    FILE * f = fopen(nomArch, "rb");
    bool b2;
    int i2;
    char c2;
    fread(&b2, sizeof(bool), 1, f);
    if(b2)
        fread (&i2, sizeof(int), 1, f);
    else
        fread (&c2, sizeof(char), 1, f);


    if(b2)
        printf("\n%i",i2);
    else
        printf("\n%c",c2);
    fclose(f);
    */
}




void Levantar_Exp_Aux(Expresion e, FILE * f){


        e = new NodoExp;

        fread(&(e->esNum), sizeof(bool), 1, f);

        if(e->esNum)
            fread (&(e->valor.num), sizeof(int), 1, f);
        else
            fread (&(e->valor.operador), sizeof(char), 1, f);

        e->hizq = NULL;
        e->hder = NULL;
        if(!feof(f)){
            Levantar_Exp_Aux(e->hizq, f);
            Levantar_Exp_Aux(e->hder, f);
        }



}



