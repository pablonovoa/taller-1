#ifndef LISTADOEXP_H_INCLUDED
#define LISTADOEXP_H_INCLUDED

#include "Expresion.h"


typedef struct nodoL{
    string nombre;
	Expresion exp;
    nodoL *sig;
} NodoListado;

typedef NodoListado * ListadoExp;

void Crear (ListadoExp & lis);
bool esVacia (ListadoExp lis);
Expresion Primero (ListadoExp lis);
void Resto (ListadoExp & lis);
void InsFront (ListadoExp & lis, Expresion e);

void incertExp(ListadoExp & lis, Expresion e, int &index);
///PreCon: lis != null
Expresion buscarExp(ListadoExp lis, string nombre);
void mostrarListado(ListadoExp lis);

void borrarListaExps(ListadoExp &lis);




#endif // LISTADOEXP_H_INCLUDED
