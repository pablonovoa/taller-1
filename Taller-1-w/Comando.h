#ifndef COMANDO_H_INCLUDED
#define COMANDO_H_INCLUDED

#include "ListadoExp.h"

typedef enum {CREATE, SUM, PRODUCT, EQUALS, SHOW, EVAL, SAVE, LOAD, QUIT, ERROR} Comando;

void peridComando(ListadoExp &lis, int &index, bool &runOn);
ArrayStrings parseInput(string s);
bool validarInput(ArrayStrings);
Comando setComando(string c);
ArrayStrings extraerParams(ArrayStrings input);

bool validarComando(Comando c, ArrayStrings params);
void runComando(Comando com, ArrayStrings params, ListadoExp &lis, int &index, bool &runOn);

void create(ArrayStrings params, ListadoExp &lis, int &index);
void sum(ArrayStrings params, ListadoExp &lis, int &index);
void product(ArrayStrings params, ListadoExp &lis, int &index);
void show(ListadoExp lis);
void eval(ListadoExp lis, ArrayStrings params);
void equals(ListadoExp lis, ArrayStrings params);
void quit(ListadoExp &lis, bool &runOn);
void save(ListadoExp lis, ArrayStrings params);
void load(ListadoExp &lis, ArrayStrings params);


#endif // COMANDO_H_INCLUDED
