#include "ListadoExp.h"

void Crear (ListadoExp & lis){ lis = NULL; }
bool esVacia (ListadoExp lis){ return lis == NULL; }
Expresion Primero (ListadoExp lis){ return lis->exp; }

void Resto (ListadoExp & lis) {
    ListadoExp aux = lis;
    lis = lis->sig;
    delete aux;
}

void InsFront (ListadoExp & lis, Expresion e, string nombre) {
    ListadoExp aux = new NodoListado;
    strcop(aux->nombre, nombre);
    aux->exp = e;
    aux->sig = lis;
    lis = aux;
}


void incertExp(ListadoExp & lis, Expresion e, int &index){
    if(e != NULL){
        string nombre = new char[MAX];
        nombre[0] = 'e';
        nombre[1] = '\0';
        strcon(nombre, numAstring(index));
        InsFront(lis, e, nombre);
        index++;
        strdestruir(nombre);
    }
}

Expresion buscarExp(ListadoExp lis, string nombre){
    if(streq(lis->nombre, nombre))
        return lis->exp;
    else if(lis->sig == NULL)
        return NULL;
    else
        return buscarExp(lis->sig, nombre);
}

void mostrarListado(ListadoExp lis){
    if(lis!=NULL){
        mostrarListado(lis->sig);
        printf("\n");
        print(lis->nombre);
        printf(": ");
        mostrarExpresion(lis->exp);
    }
}

void borrarListaExps(ListadoExp &lis){
    if(lis!=NULL){
        borrarListaExps(lis->sig);
        borrarExp(lis->exp);
        delete lis;
    }
}



