#ifndef EXPRESION_H_INCLUDED
#define EXPRESION_H_INCLUDED

#include "ArrayStrings.h"
#include "Error.h"

typedef struct nodoA{
	//string nombre;
	bool esNum;
	union{
        int num;
        char operador;
    } valor;
    nodoA *hizq;
    nodoA *hder;
} NodoExp;

typedef NodoExp * Expresion;


void Crear(Expresion &e);
bool EsVacio (Expresion e);
Expresion HijoIzq (Expresion e);
Expresion HijoDer (Expresion e);

Expresion crearExpAtomica(string s);
Expresion crearExpOperador(char op, Expresion expIzq, Expresion expDer);

void mostrarExpresion(Expresion e);

int evalExp(Expresion e, int x);
bool equalsExp(Expresion e1, Expresion e2);
bool equalsNodo(Expresion e1, Expresion e2);

void borrarExp(Expresion &e);


void Bajar_Exp(string nomArch, Expresion e);
void Bajar_Exp_Aux(Expresion e, FILE * f);
void Levantar_Exp(string nomArch, Expresion &e);
void Levantar_Exp_Aux(Expresion e, FILE * f);


#endif // EXPRESION_H_INCLUDED
