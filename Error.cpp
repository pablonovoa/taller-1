#include "Error.h"

void tirarError(int e){
    printf("ERROR: ");
    switch(e){
        case NO_INPUT:
            printf("Debe ingresar una entrada de tipo 'nombreComando param1 param2 ...' ");
        break;
        case WRONG_COMMAND:
            printf("El comando no existe");
        break;
        case WRONG_PARAMS_COUNT:
            printf("La cantidad de parametros es incorrecta");
        break;
        case EXPRESION_INVALIDA:
            printf("Expresion inválida");
        break;
        case LISTA_VACIA:
            printf("La lista de expresiones no puede estar vacía");
        break;
        case EXPRESION_NO_ENCONTRADA:
            printf("Expresion no encontrada");
        break;
        case NO_ES_NUM:
            printf("el valor de la variable no es numerico");
        break;
        case ERROR_GENERICO:
        default:
            printf("Ingrese de nuevo el comando");
        break;
    }

    printf("\n");

}
