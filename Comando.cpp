#include "Comando.h"

void peridComando(ListadoExp &lis, int &index, bool &runOn){
    string s;
    strcrear(s);
    scan(s);
    strAminusculas(s);

    ArrayStrings as = parseInput(s);


    if(as.tope == 0){
        tirarError(NO_INPUT);
    }else{
        Comando com = setComando(as.arre[0]);
        ArrayStrings params = extraerParams(as);
        bool paramsOK = validarComando(com, params);

        if(com == ERROR)
            tirarError(WRONG_COMMAND);
        else if(paramsOK)
            runComando(com, params, lis, index, runOn);
    }

    borrarArrayStrings(as);
    delete s;
}

ArrayStrings parseInput(string s){
    ArrayStrings as;
    Crear(as, 4);

    int i=0, j=0;

    string aux;
    strcrear(aux);


    while(s[i] != '\0' && as.tope < as.tamanio){
        if(s[i] != ' '){
            aux[j] = s[i];
            j++;
        }else if( j>0){
            aux[j] = '\0';
            strcop(as.arre[as.tope], aux);
            as.tope++;
            j = 0;
        }
        i++;
    }

    if( j>0){
        aux[j] = '\0';
        strcop(as.arre[as.tope], aux);
        as.tope++;
        j = 0;
    }

    delete aux;

    return as;
}

Comando setComando(string c){
    Comando com = ERROR;

    char createInputText[7] = "create";
    char sumInputText[4] = "sum";
    char productInputText[8] = "product";
    char showInputText[5] = "show";
    char evalInputText[5] = "eval";
    char equalsInputText[7] = "equals";
    char quitInputText[5] = "quit";
    char saveInputText[5] = "save";
    char loadInputText[5] = "load";

    if( streq(c, createInputText)  )
        com = CREATE;
    else if( streq(c, sumInputText)  )
        com = SUM;
    else if( streq(c, productInputText)  )
        com = PRODUCT;
    else if( streq(c, showInputText)  )
        com = SHOW;
    else if( streq(c, evalInputText)  )
        com = EVAL;
    else if( streq(c, equalsInputText)  )
        com = EQUALS;
    else if( streq(c, quitInputText)  )
        com = QUIT;
    else if( streq(c, saveInputText)  )
        com = SAVE;
    else if( streq(c, loadInputText)  )
        com = LOAD;

    return com;
}

ArrayStrings extraerParams(ArrayStrings input){
    int i;
    ArrayStrings aux;
    Crear(aux, 2);

    for(i=1; i<input.tope; i++){
        aux.arre[aux.tope] = input.arre[i];
        aux.tope++;
    }
    return aux;
}

bool validarComando(Comando c, ArrayStrings params){
    bool esOk = false;
    Error err = ERROR_GENERICO;

    switch(c){
        case CREATE:
            esOk = (params.tope == 1);
            err = WRONG_PARAMS_COUNT;
        break;
        case SUM:
            esOk = (params.tope == 2);
            err = WRONG_PARAMS_COUNT;
        break;
        case PRODUCT:
            esOk = (params.tope == 2);
            err = WRONG_PARAMS_COUNT;
        break;
        case SHOW:
            esOk = (params.tope == 0);
            err = WRONG_PARAMS_COUNT;
        break;
        case EVAL:
            esOk = (params.tope == 2 && strIsNum(params.arre[1]));
            err = (params.tope != 2)? WRONG_PARAMS_COUNT: NO_ES_NUM;
        break;
        case EQUALS:
            esOk = (params.tope == 2);
            err = WRONG_PARAMS_COUNT;
        break;
        case QUIT:
            esOk = (params.tope == 0);
            err = WRONG_PARAMS_COUNT;
        break;
        case SAVE:
            esOk = (params.tope == 2);
            err = WRONG_PARAMS_COUNT;
        break;
        case LOAD:
            esOk = (params.tope == 1);
            err = WRONG_PARAMS_COUNT;
        break;
        case ERROR:
            esOk = false;
        break;
    }

    if(!esOk)
        tirarError(err);
    return esOk;
}

void runComando(Comando com, ArrayStrings params, ListadoExp &lis, int &index, bool &runOn){
    switch(com){
        case CREATE:
            create(params, lis, index);
        break;
        case SUM:
            sum(params, lis, index);
        break;
        case PRODUCT:
            product(params, lis, index);
        break;
        case SHOW:
            show(lis);
        break;
        case EVAL:
            eval(lis, params);
        break;
        case EQUALS:
            equals(lis, params);
        break;
        case QUIT:
            quit(lis, runOn);
        break;
        case SAVE:
            save(lis, params);
        break;
        case LOAD:
            load(lis, params);
        break;
        default:
            tirarError(ERROR_GENERICO);
        break;
    }
}

void create(ArrayStrings params, ListadoExp &lis, int &index){
    Expresion e = crearExpAtomica(params.arre[0]);
    incertExp(lis, e, index);
}

void sum(ArrayStrings params, ListadoExp &lis, int &index){
    Expresion e1;
    Expresion e2;
    if(lis == NULL){
        tirarError(LISTA_VACIA);
    }else{
        e1 = buscarExp(lis, params.arre[0]);
        e2 = buscarExp(lis, params.arre[1]);
        if(e1==NULL || e2==NULL){
            tirarError(EXPRESION_NO_ENCONTRADA);
        }else{
            Expresion nueva = crearExpOperador('+', e1, e2);
            incertExp(lis, nueva, index);
        }
    }
}

void product(ArrayStrings params, ListadoExp &lis, int &index){
    Expresion e1;
    Expresion e2;
    if(lis == NULL){
        tirarError(LISTA_VACIA);
    }else{
        e1 = buscarExp(lis, params.arre[0]);
        e2 = buscarExp(lis, params.arre[1]);
        if(e1==NULL || e2==NULL){
            tirarError(EXPRESION_NO_ENCONTRADA);
        }else{
            Expresion nueva = crearExpOperador('*', e1, e2);
            incertExp(lis, nueva, index);
        }
    }
}

void show(ListadoExp lis){
    printf("\n----- Expresiones ------");
    mostrarListado(lis);
    printf("\n----------------------\n");
}

void eval(ListadoExp lis, ArrayStrings params){
    Expresion e = buscarExp(lis, params.arre[0]);
    int resultado;
    int x = strAnum(params.arre[1]);

    if(e==NULL){
        tirarError(EXPRESION_NO_ENCONTRADA);
    }else{
        resultado = evalExp(e, x);
        printf("\n");
        print(params.arre[0]);
        printf(" = %i\n", resultado);
    }
}

void equals(ListadoExp lis, ArrayStrings params){
    Expresion e1 = buscarExp(lis, params.arre[0]);
    Expresion e2 = buscarExp(lis, params.arre[1]);

    if(e1 == NULL || e2 == NULL){
        tirarError(EXPRESION_NO_ENCONTRADA);
    }else{
        if(equalsExp(e1, e2))
            printf("Las expresiones son iguales\n");
        else
            printf("Las expresiones son distintas\n");
    }
}

void quit(ListadoExp &lis, bool &runOn){
    borrarListaExps(lis);
    runOn = false;
}
void save(ListadoExp lis, ArrayStrings params){
    char extension[10] = ".data";
    string nomArch = params.arre[1];
    strcon(nomArch, extension);

    Expresion e = buscarExp(lis, params.arre[0]);

    if(e == NULL){
        tirarError(EXPRESION_NO_ENCONTRADA);
    }else{
        Bajar_Exp(nomArch, e);

    }

}

void load(ListadoExp &lis, ArrayStrings params){
    Levantar_Exp();
/*
    char extension[10] = ".data";
    string nomArch = params.arre[1];
    strcon(nomArch, extension);
    Expresion e;
    Crear(e);

    Levantar_Exp();

    mostrarExpresion(e);

*/

}

